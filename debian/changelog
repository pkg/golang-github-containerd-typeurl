golang-github-containerd-typeurl (2.1.1-3+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Mar 2025 18:38:37 +0000

golang-github-containerd-typeurl (2.1.1-3) unstable; urgency=medium

  * Team upload.
  * Upload to unstable

 -- Reinhard Tartler <siretart@tauware.de>  Fri, 26 Jul 2024 07:12:18 +0200

golang-github-containerd-typeurl (2.1.1-2) experimental; urgency=medium

  * Team upload
  * Drop unnecessary dependency version constraints
  * Bump standards version
  * Add usr/share/gocode/src/github.com/containerd/typeurl/v2 compat link

 -- Reinhard Tartler <siretart@tauware.de>  Wed, 10 Jul 2024 21:13:32 -0400

golang-github-containerd-typeurl (2.1.1-1) experimental; urgency=medium

  * Team upload.
  * New upstream version 2.1.1
  * Bump Standards version, no changes needed

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 20 May 2024 07:02:38 -0400

golang-github-containerd-typeurl (1.0.2-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Sat, 01 Apr 2023 01:06:19 +0000

golang-github-containerd-typeurl (1.0.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.0.2
  * Update Section to golang
  * Update Standards-Version to 4.6.0 (no changes)

 -- Shengjing Zhu <zhsj@debian.org>  Sun, 10 Oct 2021 02:01:59 +0800

golang-github-containerd-typeurl (1.0.1-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 16:12:54 +0000

golang-github-containerd-typeurl (1.0.1-1) unstable; urgency=low

  * Team upload.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Shengjing Zhu ]
  * New upstream version 1.0.1
  * Update debhelper compat to 13

 -- Shengjing Zhu <zhsj@debian.org>  Sun, 23 Aug 2020 21:34:50 +0800

golang-github-containerd-typeurl (1.0.0-1) unstable; urgency=medium

  * Team upload.

  [ Arnaud Rebillout ]
  * Update build dependency golang-github-gogo-protobuf-dev.

  [ Shengjing Zhu ]
  * New upstream version 1.0.0
  * Update maintainer address to team+pkg-go@tracker.debian.org
  * Update debhelper compat to 12
  * Add Rules-Requires-Root
  * Update Standards-Version to 4.5.0 (no changes)
  * Remove unused Files-Excluded in d/copyright
  * Remove ignore-marshal-test.patch.
    Not needed in new version

 -- Shengjing Zhu <zhsj@debian.org>  Thu, 16 Apr 2020 11:44:20 +0800

golang-github-containerd-typeurl (0.0~git20170912.f694355-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 15 Feb 2021 11:49:31 +0000

golang-github-containerd-typeurl (0.0~git20170912.f694355-1) unstable; urgency=medium

  * Initial release (Closes: #891181)

 -- Arnaud Rebillout <arnaud.rebillout@collabora.com>  Thu, 22 Feb 2018 16:59:41 +0700
